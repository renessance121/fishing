// interface IPurePhone {
//   number: string | undefined
// }

interface IPurePhone {
  (number:string | undefined): string;
}

export const PurePhone:IPurePhone  = (phoneNumber) => {
  if (phoneNumber) {
    return phoneNumber.replace(/\D/g, '');
  }
  return '';
};
