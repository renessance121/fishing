import i18n from 'i18next';
import { initReactI18next } from 'react-i18next';
import LanguageDetector from 'i18next-browser-languagedetector';


i18n

  .use(LanguageDetector)

  .use(initReactI18next)

  .init({
    debug: false,
    fallbackLng: 'en',
    interpolation: {
      escapeValue: false, // not needed for react as it escapes by default
    },
    resources: {
      en: {
        translation: {
          header: {
            welcomeMessage: 'Default welcome msg!-1',
            myAccountTitle: 'My Account-1',
            myAccount:{ register:'Register-1', shoppingCart:'Shopping Cart-1', checkout: 'Checkout-1' },
            language: 'EN',
            compare: 'compare (0) - 1',
            myWishList: 'My Wish List (0) - 1',
            signIn: 'Sign In - 1',
            phoneTitle:'Phone: - 1 ',
            scheduleTime:'We are open: Mn-Fr: 10 am - 8 pm(1)',
            countTitle:'count',
            eur:'eur-1',
            pounds:'pounds-1',
            dollar:'dollar-1',
          },
        },
      },
      ru: {
        translation: {
          header: {
            welcomeMessage: 'Default welcome msg!-2',
            myAccountTitle: 'My Account-2',
            myAccount:{ register:'Register-2', shoppingCart:'Shopping Cart-2', checkout: 'Checkout-2' },
            language: 'RU',
            compare: 'compare (0) - 2',
            myWishList: 'My Wish List (0) - 2',
            signIn: 'Sign In - 2',
            phoneTitle:'Phone: - 2 ',
            scheduleTime:'We are open: Mn-Fr: 10 am - 8 pm(2)',
            countTitle:'count-2',
            eur:'eur-2',
            pounds:'pounds-2',
            dollar:'dollar-2',
          },
        },
      },
      ua: {
        translation: {
          header: {
            welcomeMessage: 'Default welcome msg!-3',
            myAccountTitle: 'My Account-3',
            myAccount:{ register:'Register-3', shoppingCart:'Shopping Cart-3', checkout: 'Checkout-3' },
            language: 'UA',
            compare: 'compare (0) - 3',
            myWishList: 'My Wish List (0) - 3',
            signIn: 'Sign In - 3',
            phoneTitle:'Phone: - 3 ',
            scheduleTime:'We are open: Mn-Fr: 10 am - 8 pm(3)',
            countTitle:'count-3',
            eur:'eur-3',
            pounds:'pounds-3',
            dollar:'dollar-3',
          },
        },
      },
    },
  });

export default i18n;

// myAccount:['Register-1', 'Shopping Cart-1', 'Checkout-1'],