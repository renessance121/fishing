import React, { useCallback, useState } from 'react';
import SelectLinks from '../select/SelectLinks';
// import { optionCount } from '../../mockData/selectData';
import OptionsLinks from '../Options/OptionsLinks/OptionsLinks';
import NavigationInformation from './NavigationInformation';
import CallIcon from '@material-ui/icons/Call';
import QueryBuilderIcon from '@material-ui/icons/QueryBuilder';
import OptionsLanguage from '../Options/OptionLanguage/OptionsLanguage';
import { useTranslation } from 'react-i18next';
import classes from './topNavigation.module.scss';


const TopNavigation = () => {
  const { t } = useTranslation();

  const [toggleAccount, setToggleAccount] = useState<boolean>(true);
  const [toggleLanguage, setToggleLanguage] = useState<boolean>(true);
  const [toggleCount, setToggleCount] = useState<boolean>(true);

  const optionSelect = [
    t('header.myAccount.register'),
    t('header.myAccount.shoppingCart'),
    t('header.myAccount.checkout'),
  ];

  const optionCount = [
    t('header.eur'),
    t('header.pounds'),
    t('header.dollar'),
  ];

  const handleDropDawnLanguage = () => {
    setToggleLanguage(!toggleLanguage);
  };
  const handleDropDawnAccount = () => {
    setToggleAccount(!toggleAccount);

  };
  const handleDropDawnCount = () => {
    setToggleCount(!toggleCount);
  };
  const handleClickAwayAccount = useCallback(() => {
    if (toggleAccount == false) {
      console.log('handleClickAwayAccount');
      setToggleAccount(true);
    }
  }, [toggleAccount]);

  const handleClickAwayLanguage = useCallback(() => {
    if (toggleLanguage == false) {
      console.log('handleClickAwayLanguage');
      setToggleLanguage(true);
    }
  }, [toggleLanguage]);
  const handleClickAwayCount = useCallback(() => {
    if (toggleCount == false) {
      console.log('handleClickAwayAccount');
      setToggleCount(true);
    }
  }, [toggleCount]);

  return (
    <div className={classes.topNavigationWrapper}>
      <div className={classes.topNavigation}>
        <div className={classes.leftColumn}>
          <NavigationInformation title={t('header.phoneTitle')} firstPhone={'+38 (099) 649-05-45'}
            secondPhone={'+38 (068) 924-64-00'}>
            <CallIcon/>
          </NavigationInformation>
          <NavigationInformation title={t('header.scheduleTime')}>
            <QueryBuilderIcon/>
          </NavigationInformation>
        </div>

        <div className={classes.rightColumn}>
          <p>{t('header.welcomeMessage')}</p>
          <p>{t('header.compare')}</p>
          <SelectLinks title={t('header.myAccountTitle')} handleDropDawn={handleDropDawnAccount}
            handleClickAway={handleClickAwayAccount}
            toggleSelect={toggleAccount}>
            <OptionsLinks handleDropDawn={handleDropDawnAccount} optionSelect={optionSelect}
            />
          </SelectLinks>

          <p> {t('header.myWishList')} </p>
          <p> {t('header.signIn')} </p>
          <SelectLinks title={t('header.language')} handleDropDawn={handleDropDawnLanguage}
            handleClickAway={handleClickAwayLanguage}
            toggleSelect={toggleLanguage}>
            <OptionsLanguage handleDropDawn={handleDropDawnLanguage}/>
          </SelectLinks>
          <SelectLinks title={t('header.countTitle')} handleDropDawn={handleDropDawnCount}
            handleClickAway={handleClickAwayCount}
            toggleSelect={toggleCount}
          >
            <OptionsLinks handleDropDawn={handleDropDawnCount} optionSelect={optionCount}
            />
          </SelectLinks>
        </div>

      </div>
    </div>
  );
};

export default TopNavigation;
