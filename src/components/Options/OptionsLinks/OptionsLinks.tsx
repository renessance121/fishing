import React, { useState } from 'react';
import classes from '../Options.module.scss';
import clsx from 'clsx';
import { NavLink } from 'react-router-dom';

interface ISelect {
  optionSelect: Array<string>,
  handleDropDawn: () => void,
}

const OptionsLinks: React.FC<ISelect> = ({ handleDropDawn, optionSelect }) => {

  const [toggleClass, setToggleClass] = useState<null | number>(null);
  const handletoggleClass = (index: number) => {
    setToggleClass(index);
  };

  return (

    <div className={clsx(classes.dropdown)}>
      {
        optionSelect.map((item, index) => {
          const itemWithoutSpace = item.replace(' ', '');
          return <NavLink key={itemWithoutSpace} to={`/${itemWithoutSpace}`}
            className={clsx({ [classes.classActive]: toggleClass === index })}
            onClick={() => {
              handleDropDawn();
              return handletoggleClass(index);
            }}
          > {item} </NavLink>;
        })
      }

    </div>
  );
};

export default OptionsLinks;
